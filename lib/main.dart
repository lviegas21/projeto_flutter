import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'package:loadmore/loadmore.dart';

// arrow_back 
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter',
      debugShowCheckedModeBanner: false,
      home: MyHomePage(title: 'projeto flutter'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List data;
  int get count => list.length;

  List<int> list = [];


  void load() {
    print("load");
    setState(() {
      list.addAll(List.generate(8, (v) => v));
      print("data count = ${list.length}");
    });
  }

  // Função para obter os dados JSON
  Future<String> getJSONData() async {
    var response = await http.get(
        // codifiga a url
        Uri.encodeFull("https://www.balldontlie.io/api/v1/players"),
        // Aceita somente resposta JSON
        headers: {"Accept": "application/json"}
    );

    setState(() {
      // Pega os dados JSON
      data = json.decode(response.body)['data'];
    });

    return "Dados obtidos com sucesso";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
    body: DefaultTabController(
    length: 2,
    child: NestedScrollView(
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled){
        return <Widget>[
          
          SliverAppBar(
            backgroundColor: Colors.white,
            expandedHeight: 100,
            floating: false,
            pinned: true,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: false ,
            title: Text('NBA Teams',
            style: TextStyle(color: Colors.black,
            fontSize: 16),
            ),
            ),

          ),
          
            ];
          },

      body: Container(
        child: RefreshIndicator(
          child: LoadMore(
            isFinish: count >= 24,
            onLoadMore: _loadMore,
            child: ListView.builder(
              itemBuilder: (BuildContext context, int index){
                return _colunaTeams(data[index]);
              },
              itemCount: count,
              ), 
              whenEmptyLoad: false,
              delegate: DefaultLoadMoreDelegate(),
              textBuilder: DefaultLoadMoreTextBuilder.english,
            ), 
          onRefresh: _refresh,
          ),
          ),
    ),
       ),
    );
    
}

  Future<bool> _loadMore() async {
    print("onLoadMore");
    await Future.delayed(Duration(seconds: 0, milliseconds: 2000));
    load();
    return true;
  }

  Future <void> _refresh() async {
    await Future.delayed(Duration(seconds: 0, milliseconds: 2000));
    list.clear();
    load();
  }


Widget _colunaTeams(dynamic item) => Container(
      decoration: BoxDecoration(
        color: Colors.white54
      ),
      margin: const EdgeInsets.all(4),
      child: Column(
        children: [
          _criaLinha(item)
        ],
      ),
    );

Widget _criaLinha(dynamic item) {
    return ListTile(
      
      leading: Tab(
        icon: Container(
          child: Image(
            image: AssetImage('imagem/icons8-google-48.png'),
          ),
          width: 20,
        ),
      ),
      subtitle: Text(item['first_name'].toString() + " " + item['last_name'].toString(),
      style: TextStyle(
        color: Colors.black,
        fontSize: 15,
      ),),
      title: Text(
        item['id'] == null ? '': item['id'].toString(),
        style: TextStyle(
          color: Colors.grey,
          fontSize: 12,
        ),
      ),
     
    );
  }
  @override
  void initState() {
    super.initState();
    // Chama o método getJSONData() quando a app inicializa
    this.getJSONData();
  }
}

